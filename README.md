# Texture Roller Generator for Miniatures

This Python program automates the creation of 3D-printable texture rollers. These rollers are used to imprint textures like brick or rock on malleable surfaces, ideal for terrain crafting in miniature settings. The program converts height maps into textured 3D cylinder models.

## Features

- **Load Height Map Images**: Define texture by loading height map images to be converted into textured 3D cylinder models.
- **Tiling Functionality**: Enables the repetition of texture patterns across the cylinder's surface. Utilize tiling to effectively extend the texture along the height and circumference of the roller, which allows for the creation of larger rollers without needing a larger original image. Specify the number of horizontal repeats (`--scale_radius`) to increase the cylinder's circumference, and vertical repeats (`--scale_height`) to extend the cylinder's height.
- **Adjustable Parameters**: Customize the displacement, height, and inner radius of the roller to suit different crafting needs.
- **Imprint Options**: Choose between positive and negative imprints to achieve the desired relief effect on the malleable surface.
- **Automatic Image Processing**: Handles the conversion of images to greyscale and inverts them if required, based on the selected imprint option.

## Installation

### Prerequisites

Ensure that you have Python installed on your machine. This program has been tested with Python 3.8+.

### Required Libraries

This program depends on several Python libraries including `numpy`, `imageio`, `trimesh`, and `tqdm`. You can install these using pip:

```bash
pip install numpy imageio trimesh tqdm
```

### Download and Setup

1. Clone the repository or download the source code:
   ```bash
   git clone https://gitlab.com/gillis.oldfeldt/texture-roller-generator.git
   ```
2. Navigate into the project directory:
   ```bash
   cd texture-roller-generator
   ```

## Usage

Run the script from the command line, providing the necessary arguments:

```bash
python roller.py [image_path] [output_name] [options]
```
### Options

- `--scale_height`: Number of tiles in the height direction, affects roller height. Increasing this number extends the roller's length, allowing for larger textured surfaces without a proportional increase in the original image size (default: 1).
- `--scale_radius`: Number of tiles in the radius direction, affects roller circumference. Increasing this number enlarges the roller's circumference, enabling broader textured patterns and larger rollers from the same image (default: 1).
- `--displacement`: Set the maximum displacement for texture depth. Higher values result in more pronounced texture effects on the roller surface (default: 5).
- `--height`: Total height of the cylinder. This dimension can be increased in conjunction with `--scale_height` to produce taller rollers (default: 53).
- `--inner_radius`: Inner radius of the cylinder. This setting adjusts the minimum radius within the hollow core of the roller, affecting how the roller fits onto its support structure (default: 2.5).
- `--negative_imprint`: Use this flag to invert the texture, creating a negative imprint that results in a raised design on the malleable surface rather than an indented one.

### Example

```bash
python roller.py ./textures/brick.jpg roller_output --displacement 3.5 --height 50 --scale_height 2 


## Contributing

Feel free to fork this project and make your own changes. The code is provided as-is, and I do not plan on maintaining it.

## License

This project is open source and available under the [MIT License](LICENSE.md).

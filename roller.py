import argparse
import imageio.v2 as imageio
import numpy as np
import trimesh
import os
from tqdm import tqdm

def load_image(image_path, height, width, negative_imprint=False):
    print(f"Loading image from {image_path}...")
    
    image = imageio.imread(image_path)
    
    if image.ndim == 3:
        image = np.mean(image, axis=2)
    
    max_image_value = np.max(image)
    
    if not negative_imprint:
        image = max_image_value - image
    
    if height > 1 or width > 1:
        image = np.tile(image, (height, width))
    
    return image

def create_detailed_cylinder(radius, height, radial_segments, height_segments):
    print("Creating base cylinder mesh...")
    vertices = []
    faces = []
    h_step = height / height_segments
    theta_step = 2 * np.pi / radial_segments

    for i in range(height_segments + 1):
        z = -height / 2 + i * h_step
        for j in range(radial_segments):
            theta = j * theta_step
            x = radius * np.cos(theta)
            y = radius * np.sin(theta)
            vertices.append([x, y, z])

    vertices = np.array(vertices)

    for i in range(height_segments):
        for j in range(radial_segments):
            current = i * radial_segments + j
            next_radial = current + 1 if j < radial_segments - 1 else i * radial_segments
            upper = current + radial_segments
            upper_next_radial = next_radial + radial_segments

            faces.append([current, next_radial, upper])
            faces.append([next_radial, upper_next_radial, upper])

    return trimesh.Trimesh(vertices=vertices, faces=faces)

def apply_displacement(radius, height, origmesh, image, max_displacement=0.1):
    mesh = origmesh.copy()
    radial_segments, height_segments = image.shape[1], image.shape[0]
    vertices = mesh.vertices
    new_vertices = np.empty_like(vertices)
    max_image_value = np.max(image)
    print("max_image_value:", max_image_value)
    for i in tqdm(range(len(vertices)), desc="Applying displacement"):
        x, y, z = vertices[i]
        theta = np.arctan2(y, x) + 2 * np.pi if y < 0 else np.arctan2(y, x)
        v = (theta / (2 * np.pi)) * (radial_segments - 1)
        u = ((z + height / 2) / height) * (height_segments - 1)
        u0, v0 = int(u), int(v)
        u1, v1 = min(u0 + 1, height_segments - 1), min(v0 + 1, radial_segments - 1)
        alpha, beta = u - u0, v - v0
        f00 = image[u0, v0]
        f10 = image[u1, v0]
        f01 = image[u0, v1]
        f11 = image[u1, v1]
        texture_value = (1 - alpha) * (1 - beta) * f00 + alpha * (1 - beta) * f10 + (1 - alpha) * beta * f01 + alpha * beta * f11
        displacement = (texture_value / max_image_value) * max_displacement
        new_radius = radius + displacement
        new_x = new_radius * np.cos(theta)
        new_y = new_radius * np.sin(theta)
        new_vertices[i] = [new_x, new_y, z]

    mesh.vertices = new_vertices
    mesh.smooth_shaded
    return mesh


def add_inner_surface_and_cap(outer_mesh, inner_mesh, inner_radius):
    print("Adding inner surface and caps...")
    scale_factor = inner_radius / np.max(np.linalg.norm(inner_mesh.vertices[:, :2], axis=1))
    inner_mesh.vertices[:, :2] *= scale_factor
    inner_mesh.faces = np.fliplr(inner_mesh.faces)
    combined_mesh = trimesh.util.concatenate(outer_mesh, inner_mesh)
    vertices = combined_mesh.vertices
    max_z = np.max(vertices[:, 2])
    min_z = np.min(vertices[:, 2])
    top_indices_outer = np.where(np.isclose(vertices[:, 2], max_z) & (np.linalg.norm(vertices[:, :2], axis=1) > inner_radius))[0]
    top_indices_inner = np.where(np.isclose(vertices[:, 2], max_z) & (np.linalg.norm(vertices[:, :2], axis=1) <= inner_radius))[0]
    bottom_indices_outer = np.where(np.isclose(vertices[:, 2], min_z) & (np.linalg.norm(vertices[:, :2], axis=1) > inner_radius))[0]
    bottom_indices_inner = np.where(np.isclose(vertices[:, 2], min_z) & (np.linalg.norm(vertices[:, :2], axis=1) <= inner_radius))[0]
    top_faces = []
    bottom_faces = []
    for i in range(len(top_indices_outer)):
        next_i = (i + 1) % len(top_indices_outer)
        top_faces.append([top_indices_outer[i], top_indices_inner[i], top_indices_outer[next_i]])
        top_faces.append([top_indices_inner[i], top_indices_inner[next_i], top_indices_outer[next_i]])
    for i in range(len(bottom_indices_outer)):
        next_i = (i + 1) % len(bottom_indices_outer)
        bottom_faces.append([bottom_indices_outer[i], bottom_indices_outer[next_i], bottom_indices_inner[i]])
        bottom_faces.append([bottom_indices_inner[i], bottom_indices_outer[next_i], bottom_indices_inner[next_i]])
    combined_mesh.faces = np.vstack([combined_mesh.faces, top_faces, bottom_faces])
    return combined_mesh

def generate_unique_filename(directory, filename):
    base_name, extension = os.path.splitext(filename)
    counter = 1
    unique_filename = filename  # Start with the original filename

    # Generate a new file name if the proposed one already exists
    while os.path.exists(os.path.join(directory, unique_filename)):
        unique_filename = f"{base_name}({counter}){extension}"
        counter += 1

    return unique_filename

def main():
    parser = argparse.ArgumentParser(description='Generate a textured 3D model.')
    parser.add_argument('image_path', type=str, help='Path or URL to the texture image')
    parser.add_argument('output_name', type=str, help='Output filename for the STL file')
    parser.add_argument('--scale_height', type=int, default=1, help='Number of tiles in the height direction, affects roller height (default: 1)')
    parser.add_argument('--scale_radius', type=int, default=1, help='Number of tiles in the radius direction, affects roller radius (default: 1)')
    parser.add_argument('--displacement', type=float, default=5, help='Displacement amount for texture effect (default: 3.5)')
    parser.add_argument('--height', type=float, default=53, help='Height of the cylinder (default: 53)')
    parser.add_argument('--inner_radius', type=float, default=2.5, help='Inner radius of the cylinder (default: 2)')
    parser.add_argument('--negative_imprint', action='store_true', help='Make imprint negative')
    args = parser.parse_args()

    image = load_image(args.image_path,args.scale_height, args.scale_radius, args.negative_imprint)
    outer_radius = args.height*args.scale_radius / (2 * np.pi)

    print("Generating cylinder mesh...")
    mesh = create_detailed_cylinder(outer_radius, args.height*args.scale_height, len(image[1]) // 2, len(image[0]) // 2)
    displaced_mesh = apply_displacement(outer_radius, args.height*args.scale_height, mesh, image, args.displacement)
    cylinder_mesh = add_inner_surface_and_cap(displaced_mesh, mesh, args.inner_radius)

    output_folder = 'texture_rollers'
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    # Use the function to get a unique file name
    unique_filename = generate_unique_filename(output_folder, args.output_name + '.stl')
    output_path = os.path.join(output_folder, unique_filename)

    print(f"Exporting the mesh to {output_path}...")
    cylinder_mesh.export(output_path)
    print("Export completed successfully.")

if __name__ == '__main__':
    main()